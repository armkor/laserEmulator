#pragma once

#include <string>

class IEmulator
{
public:

    IEmulator() = default;
    virtual ~IEmulator() = default;
    IEmulator(const IEmulator&) = delete;
    IEmulator & operator=(const IEmulator&) = delete;

    virtual std::string doCommand(std::string command) = 0;
};
