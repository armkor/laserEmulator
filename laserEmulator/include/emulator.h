#pragma once

#include "iEmulator.h"
#include <thread>
#include <mutex>

class Emulator: public IEmulator
{
    enum Mode
    {
        Direct,
        Reverse
    };

    enum State
    {
        Ready,
        Running
    };

    enum LaserState
    {
        EmitOn,
        EmitOff
    };

    enum Commands
    {
        NOTFOUND,
        STR, // start emit laser
        STP, // stop emit laser
        ST, // status of laser
        KAL, // keep signal alive
        PW, // current laser power
        PWSET, // set current laser power
        ESM, // set commands in reverse mode
        DSM// set commands in direct mode
    };

public:
    Emulator();
    ~Emulator();

    bool start();
    std::string doCommand(std::string command) override;

private:
    bool isEmitting();
    bool isReverseMode();

    void setEmmiting(bool on);
    void setMode(bool isDirect);

    void monitorLaser();

    Emulator::Commands parse(std::string commandStr);
    Commands parseCommand(std::string command);

    std::string makeCommand(Commands command);
    std::string makeSTR();
    std::string makeSTP();
    std::string makeST();
    std::string makeKAL();
    std::string makePW();
    std::string makePWSET();
    std::string makeESM();
    std::string makeDSM();

    std::string transformLine(std::string commandStr);

    Mode          m_mode;
    State         m_emulatorState;
    LaserState    m_state;
    unsigned int  m_laserPower;
    std::string   m_laserPowerStr;
    std::thread   m_laserKALMonitorThread;
    std::mutex    m_LasetEmitUpdatedMtx;
    std::chrono::time_point<std::chrono::system_clock> m_lastLaserEmitUpdatedTime;
};

