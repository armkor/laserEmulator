cmake_minimum_required(VERSION 3.5)

project(laserEmulator LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

include_directories(include)

set(HEADERS
    include/iEmulator.h
    include/emulator.h
)

set(SOURCES
    main.cpp
    src/emulator.cpp
)

add_executable(laserEmulator ${HEADERS} ${SOURCES})
