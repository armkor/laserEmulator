#include "emulator.h"
#include <iterator>
#include <algorithm>
#include <iostream>
#include <chrono>

Emulator::Emulator()
    : m_mode(Mode::Direct),
      m_emulatorState(State::Ready),
      m_state(LaserState::EmitOff),
      m_laserPower(1)
{

}

Emulator::~Emulator()
{
    // Can't kill while thread
    if (m_laserKALMonitorThread.joinable())
    {
        m_laserKALMonitorThread.join();
    }
}

bool Emulator::start()
{
    if (m_emulatorState == State::Ready)
    {
        m_emulatorState = State::Running;
        // Need to wait that thread is ready
        m_laserKALMonitorThread = std::thread(&Emulator::monitorLaser,this);
        return true;
    }
    else
    {
        // already started
        return false;
    }
}

std::string Emulator::doCommand(std::string commandStr)
{
    if (m_emulatorState != State::Running)
    {
        return ""; // TODO: Set internal error via command
    }

    Commands command = parse(commandStr);
    return makeCommand(command);
}

bool Emulator::isEmitting()
{
    return m_state == LaserState::EmitOn;
}

bool Emulator::isReverseMode()
{
    return m_mode == Mode::Reverse;
}

void Emulator::setEmmiting(bool on)
{
    if (on)
    {
        {
            std::lock_guard<std::mutex> lk(m_LasetEmitUpdatedMtx);
            m_lastLaserEmitUpdatedTime = std::chrono::system_clock::now();
        }
        m_state = EmitOn;
    }
    else
    {
        m_state = EmitOff;
    }
}

void Emulator::setMode(bool isDirect)
{
    if (isDirect)
    {
        m_mode = Direct;
    }
    else
    {
        m_mode = Reverse;
    }
}

void Emulator::monitorLaser()
{
    while(true)
    {
        {
            std::cout << "Test" << std::endl << std::flush;

            std::lock_guard<std::mutex> lk(m_LasetEmitUpdatedMtx);
            auto seconds = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now() - m_lastLaserEmitUpdatedTime);

            auto sec = seconds.count();

            std::cout << "Sec:" << sec << std::flush;

            if (sec > 5)
            {
                std::cout << "Switch off laser" << std::endl << std::flush;
                m_state = EmitOff;
            }
        }
        std::this_thread::sleep_for(std::chrono::seconds(3));
    }
}

Emulator::Commands Emulator::parse(std::string commandStr)
{
    commandStr = transformLine(commandStr);

    std::string::size_type commandStartPosition = commandStr.find('|');
    if (commandStartPosition != std::string::npos) {
        m_laserPowerStr = commandStr.substr(commandStartPosition + 1);
        commandStr = commandStr.substr(0, commandStartPosition);
    }

    return parseCommand(commandStr);
}

Emulator::Commands Emulator::parseCommand(std::string command)
{
    if (command.compare("STR") == 0)
    {
        return Commands::STR;
    }
    else if (command.compare("STP") == 0)
    {
        return Commands::STP;
    }
    else if (command.compare("ST?") == 0)
    {
        return Commands::ST;
    }
    else if (command.compare("KAL") == 0)
    {
        return Commands::KAL;
    }
    else if (command.compare("PW?") == 0)
    {
        return Commands::PW;
    }
    else if (command.compare("PW=") == 0)
    {
        return Commands::PWSET;
    }
    else if (command.compare("STP") == 0)
    {
        return Commands::STP;
    }
    else if (command.compare("ESM") == 0)
    {
        return Commands::ESM;
    }
    else if (command.compare("DSM") == 0)
    {
        return Commands::DSM;
    }

    return Commands::NOTFOUND;
}

std::string Emulator::makeCommand(Emulator::Commands command)
{
    switch (command)  {
        case STR:
            return makeSTR();
            break;
        case STP:
            return makeSTP();
            break;
        case ST:
            return makeST();
            break;
        case KAL:
            return makeKAL();
            break;
        case PW:
            return makePW();
            break;
        case PWSET:
            return makePWSET();
            break;
        case ESM:
            return makeESM();
            break;
        case DSM:
            return makeDSM();
            break;
    }
    return "UK!";
}

std::string Emulator::makeSTR()
{
    if (isEmitting())
    {
        return "STR!";
    }
    else
    {
        setEmmiting(true);
        return "STR#";
    }
}

std::string Emulator::makeSTP()
{
    if (!isEmitting())
    {
        return "STP!";
    }
    else
    {
        setEmmiting(false);
        return "STP#";
    }
}

std::string Emulator::makeST()
{
    if (isEmitting())
    {
        return "ST?|1#";
    }
    else
    {
        return "ST?|0#";
    }
}

std::string Emulator::makeKAL()
{
    if (isEmitting())
    {
        setEmmiting(true);
        return "KAL#";
    }
    else
    {
        return "KAL!";
    }
}

std::string Emulator::makePW()
{
    if (isEmitting())
    {
        return "PW?|" + std::to_string(m_laserPower) + "#";
    }
    else
    {
        return "PW?|0#";
    }
}

std::string Emulator::makePWSET()
{
    if (isEmitting())
    {
        int laserPower;
        try
        {
            // TODO: Need to convert manually. Can be garbadge after digits. See test 5
            laserPower = std::stoi(m_laserPowerStr);
            if (laserPower >= 1 && laserPower <= 100)
            {
                m_laserPower = laserPower;
                return "PW=#";
            }
        }
        catch (...) {

        }
    }
    return "PW=!";
}

std::string Emulator::makeESM()
{
    setMode(false);
    return "ESM#";
}

std::string Emulator::makeDSM()
{
    setMode(true);
    return "DSM#";
}

std::string Emulator::transformLine(std::string commandStr)
{
    if (isReverseMode())
    {
        std::reverse(commandStr.begin(), commandStr.end());
    }

    return commandStr;
}
