#include <iostream>
#include <chrono>
#include <thread>
#include "emulator.h"

using namespace std;

bool checkResult(std::string result, std::string expected)
{
    bool isError = (result.compare(expected) != 0);
    if (isError)
    {
        std::cout << "Result command "<< result << " is not " << expected << '\n';
    }
    else
    {
        std::cout << "Command " << expected << "correct" << '\n';
    }
    return !isError;
}

void test1()
{
    std::cout << "Test 1 started\n";
    bool isOk = true;
    std::string result;
    Emulator emulator;
    emulator.start();
    std::this_thread::sleep_for(std::chrono::seconds(6));
    isOk &= checkResult(emulator.doCommand("STR"), "STR#");
    isOk &= checkResult(emulator.doCommand("PW=|50"), "PW=#");
    isOk &= checkResult(emulator.doCommand("PW?"), "PW?|50#");
    isOk &= checkResult(emulator.doCommand("STP"), "STP#");
    isOk &= checkResult(emulator.doCommand("PW=|20"), "PW=!");

    std::cout << "Test 1 finished with status:" << isOk << std::endl << std::flush;
}

void test2()
{
    std::cout << "Test 2 started\n";

    bool isOk = true;
    std::string result;
    Emulator emulator;
    emulator.start();
    isOk &= checkResult(emulator.doCommand("HLO"), "UK!");

    std::cout << "Test 2 finished with status:" << isOk << std::endl << std::flush;
}

void test3()
{
    std::cout << "Test 3 started\n";

    bool isOk = true;
    std::string result;
    Emulator emulator;
    emulator.start();
    isOk &= checkResult(emulator.doCommand("STR"), "STR#");
    isOk &= checkResult(emulator.doCommand("ESM"), "ESM#");
    isOk &= checkResult(emulator.doCommand("05|=WP"), "PW=#");
    isOk &= checkResult(emulator.doCommand("MSD"), "DSM#");
    isOk &= checkResult(emulator.doCommand("PW?"), "PW?|50#");

    std::cout << "Test 3 finished with status:" << isOk << std::endl << std::flush;
}

void test4()
{
    std::cout << "Test 4 started\n";

    bool isOk = true;
    std::string result;
    Emulator emulator;
    emulator.start();
    isOk &= checkResult(emulator.doCommand("STR"), "STR#");
    isOk &= checkResult(emulator.doCommand("ST?"), "ST?|1#");
    isOk &= checkResult(emulator.doCommand("KAL"), "KAL#");
    isOk &= checkResult(emulator.doCommand("ST?"), "ST?|1#");
    std::this_thread::sleep_for(std::chrono::seconds(6));
    isOk &= checkResult(emulator.doCommand("ST?"), "ST?|0#");

    std::cout << "Test 4 finished with status:" << isOk << std::endl << std::flush;
}

void test5()
{
    std::cout << "Test 5 started\n";

    bool isOk = true;
    std::string result;
    Emulator emulator;
    emulator.start();
    isOk &= checkResult(emulator.doCommand("STR"), "STR#");
    isOk &= checkResult(emulator.doCommand("PW=|50%^&"), "PW=#");
    isOk &= checkResult(emulator.doCommand("PW?"), "PW?|1");

    std::cout << "Test 5 finished with status:" << isOk << std::endl << std::flush;
}

void makeTests()
{
    test4();
    test2();
    test3();
    test4();
    test5();
}

int main(int argc, char *argv[])
{
    bool testMode = argc > 1;
    if (testMode)
    {
       makeTests();
       return 1;
    }

    Emulator emulator;
    emulator.start();

    for (std::string line; std::getline(std::cin, line);) {
        std::cout << emulator.doCommand(line) << std::endl;
    }

    return 0;
}
